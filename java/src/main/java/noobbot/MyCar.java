package noobbot;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

enum SwitchTarget {RIGHT, LEFT, NONE}

public class MyCar extends Car {
	public Track track = null; // track info
	public PiecePosition currPP = null;// present position
	public double angle = 0.0; // slipping degree
	public List<Piece> fowardPieces = null;
	double slottle = 0.0;
	boolean turbo = false;
	double round = 0.0;
	double curveRate = 0.0;
	SwitchTarget st = SwitchTarget.NONE;

	public MyCar() {
	}

	// =========================
	// Setters
	// =========================

	public void setRaceInfo(Track track) {
		this.track = track;
		calcCourseInfo();
	}

	public void setMyCarInfo(YourCar yourcar) {
		IdRace ir = new IdRace(yourcar.name, yourcar.color);
		this.id = ir;
	}

	public void setPiecePosition(PiecePosition newPos) {
		this.currPP = newPos;
		this.fowardPieces = getForwardPieces(newPos.getPieceIndex(), 10);
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public void setCurrPP(PiecePosition currPP) {
		this.currPP = currPP;
	}

	public void setFowardPieces(List<Piece> fowardPieces) {
		this.fowardPieces = fowardPieces;
	}

	public void setSlottle(double slottle) {
		this.slottle = slottle;
	}

	// ==========================
	// Methods
	// ==========================

	/**
	 * get info about coming n pieces of course
	 * 
	 * @param currPiece
	 * @param n
	 * @return List<Piece>
	 */
	public List<Piece> getForwardPieces(int currPiece, int n) {
		List<Piece> forwardPieces = new ArrayList<Piece>();
		// get total number of pieces in 1 track
		int trackSize = this.track.pieces.size();
		for (int i = currPiece; i < currPiece + n; i++) {
			int targetPiece = i % trackSize;
			forwardPieces.add(track.pieces.get(targetPiece));
		}
		return forwardPieces;
	}
	
	private void calcCourseInfo() {
		double sum = 0.0;
		int curveCount = 0;
		int trackSize = this.track.pieces.size();
		for (int i = 0; i < trackSize; i++) {
			sum += track.pieces.get(i).length;
			if (track.pieces.get(i).radius != 0.0) {
				curveCount++;
			}
		}
		this.round = sum;
		this.curveRate = (double)curveCount / (double)trackSize;
//		System.out.println(curveRate);
	}

	/**
	 * calculate speed(by 1tick)<br>
	 * after calc, set the newPP in currPP
	 * 
	 * @param newPP
	 * @return
	 */
	public double calcSpeed(PiecePosition newPP) {
		double speed = 0.0;

		// if not the first time access
		if (this.currPP != null) {

			int currPieceIndex = currPP.getPieceIndex();
			int newPieceIndex = newPP.getPieceIndex();
			double currDist = currPP.getInPieceDistance();
			double newDist = newPP.getInPieceDistance();

			// if on the same piece
			if (newPieceIndex == currPieceIndex) {
				speed = (newDist - currDist);
			} else {
				// if on the different piece, dist = currPieceLength - currDist
				// + newDist)
				// difference between piece indices was assumed to be at most 1
				speed = (track.pieces.get(currPieceIndex).length - currDist + newDist);
			}

		}
		// update current position
		setPiecePosition(newPP);
		return speed;
	}

	public double decideSpeed() {
//		int count = getDistanceUntilBending();
		double distance = getDistanceUntilBending();
//		System.out.println("Distance until bending is " + distance);
		
		// If curve rate in course is high, the base speed(left expression) should be high.
		this.slottle = (1.0 - curveRate + 0.2) + distance / 600;
//		this.slottle = 1.0;
//		if (distance > 500) {
//			this.slottle = 1.0;
//		}
		if (this.slottle > 1.0) {
			this.slottle = 1.0;
		}
		return this.slottle;
	}

	public void decideTurbo() {
//		int count = getDistanceUntilBending();
		double distance = getDistanceUntilBending();
//		System.out.println(distance);
		// Reconsider
		if (distance > 700) {
			this.turbo = true;
		} else {
			this.turbo = false;
		}
	}
	
	private double getDistanceUntilBending() {
		double distance = fowardPieces.get(0).length - currPP.getInPieceDistance();

		Iterator<Piece> pieceItr = fowardPieces.iterator();
		Piece nextPiece = pieceItr.next();
		while (pieceItr.hasNext()) {
			nextPiece = pieceItr.next();
			if (nextPiece.radius != 0.0) {
				return distance;
			}
			distance += nextPiece.length;
		}
		return distance;
	}

	public void decideSwitchTarget() {
		if (this.angle > 0.0) {
			this.st = SwitchTarget.RIGHT;
		} else if (this.angle < 0.0) {
			this.st = SwitchTarget.LEFT;
		} else {
			this.st = SwitchTarget.NONE;
		}
	}
}
