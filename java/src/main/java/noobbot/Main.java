package noobbot;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class Main {
	static String COURSE_NAME;

	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		// System.out.println("Connecting to " + host + ":" + port + " as "
		// + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(
				socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(
				socket.getInputStream(), "utf-8"));
		if (args.length > 4) {
			String course = args[4];
			int cars = Integer.parseInt(args[5]);
			COURSE_NAME = course;
			new Main(reader, writer,
					new JoinRace(botName, botKey, course, cars));
		} else {
			new Main(reader, writer, new Join(botName, botKey));
		}
	}

	final Gson gson = new Gson();
	private PrintWriter writer;

	/**
	 * Main class for automated test racing.
	 * 
	 * @param reader
	 * @param writer
	 * @param join
	 * @throws IOException
	 */
	public Main(final BufferedReader reader, final PrintWriter writer,
			final Join join) throws IOException {
		this.writer = writer;
		String line = null;

		send(join);

		// generate mycar
		MyCar mycar = new MyCar();
		String myCarName;
		double thlottle = 0.7;
		boolean turboAvailable = false;
		mycar.setSlottle(thlottle);

		while ((line = reader.readLine()) != null) {
			final MsgWrapper msgFromServer = gson.fromJson(line,
					MsgWrapper.class);

			if (msgFromServer.msgType.equals("carPositions")) {
				// set present position in my car
				myCarName = mycar.id.name;
				CarPositions carPositions = gson.fromJson(line,
						CarPositions.class);
				Data carPositionsData = carPositions
						.getSpecifiedDataForCarNamed(myCarName);

				/* Get variables. */
				double angle = carPositionsData.getAngle();
				PiecePosition currentPiecePosition = carPositionsData
						.getPiecePosition();
				decideAction(currentPiecePosition, turboAvailable);

				// /* Set variables. */
//				 mycar.setAngle(angle);
//				 mycar.setPiecePosition(currentPiecePosition);
//				 mycar.decideTurbo();
//				 mycar.decideSwitchTarget();
//				//
//				 thlottle = mycar.decideSpeed();
//				 mycar.setSlottle(thlottle);
//				//
//				 decideNextAction(mycar, thlottle, turboAvailable);
			} else if (msgFromServer.msgType.equals("turboAvailable")) {
				System.out.println("Turbo available!");
				turboAvailable = true;
			} else if (msgFromServer.msgType.equals("join")) {
				// System.out.println("Joined");
			} else if (msgFromServer.msgType.equals("gameInit")) {
				System.out.println("Race init");
				// set track info in my car
				Course course = gson.fromJson(gson.toJson(msgFromServer.data),
						Course.class);
				mycar.setRaceInfo(course.race.track);
				// logData.setCourseInfo(course);
			} else if (msgFromServer.msgType.equals("gameEnd")) {
				System.out.println("Race end");
				// String jsonForLogging = gson.toJson(logData);
				// System.out.println(jsonForLogging);
			} else if (msgFromServer.msgType.equals("gameStart")) {
				send(new Throttle(1.0));
				System.out.println("Race start");
			} else if (msgFromServer.msgType.equals("yourCar")) {
				// set own car info in my car
				YourCar yourcar = gson.fromJson(
						gson.toJson(msgFromServer.data), YourCar.class);
				mycar.setMyCarInfo(yourcar);
			} else if (msgFromServer.msgType.equals("crash")) {
				System.out.println("Crash!");
			} else if (msgFromServer.msgType.equals("spawn")) {
				System.out.println("Spawn!");
			} else if (msgFromServer.msgType.equals("lapFinished")) {
				System.out.println("Lap finished");
			} else {
				send(new Ping());
			}
		}
	}

	private void decideAction(PiecePosition currentPiecePosition, boolean turboAvailable) {
		int pieceIndex = currentPiecePosition.getPieceIndex();

		switch (pieceIndex) {
		case 0:
			send(new Throttle(1.0));
			break;
		case 1:
			send(new Throttle(0.60));
			break;
		case 2:
			send(new Throttle(0.3));
			break;
		case 3:
			send(new Throttle(0.3));
//			send(new SwitchLane("Left"));
			break;
		case 4:
			send(new Throttle(0.5));
			break;
		case 5:
			send(new Throttle(0.60));
			break;
		case 6:
			send(new Throttle(0.66));
			break;
		case 7:
			send(new Throttle(0.7));
			break;
		case 8:
			send(new SwitchLane("Left"));
			break;
		case 9:
			send(new Throttle(0.8));
			break;
		case 10:
			send(new Throttle(0.8));
			break;
		case 11:
			send(new Throttle(0.8));
			break;
		case 12:
			send(new Throttle(0.6));
			break;
		case 13:
			send(new Throttle(0.6));
			break;
		case 14:
			send(new Throttle(0.6));
			break;
		case 15:
			send(new Throttle(0.6));
			break;
		case 16:
			send(new Throttle(0.6));
			break;
		case 17:
			send(new Throttle(0.6));
			break;
		case 18:
			send(new SwitchLane("Right"));
			break;
		case 19:
			send(new Throttle(0.75));
			break;
		case 20:
			send(new Throttle(0.75));
			break;
		case 21:
			send(new Throttle(0.75));
			break;
		case 22:
			send(new Throttle(0.75));
			break;
		case 23:
			send(new Throttle(0.8));
			break;
		case 24:
			send(new Throttle(0.7));
			break;
		case 25:
			send(new Throttle(0.75));
			break;
		case 26:
			send(new Throttle(0.4));
			break;
		case 27:
			send(new Throttle(0.6));
			break;
		case 28:
			send(new Throttle(0.6));
			break;
		case 29:
			send(new Throttle(0.6));
			break;
		case 30:
			send(new Throttle(0.6));
			break;
		case 31:
			send(new Throttle(0.6));
			break;
		case 32:
			send(new Throttle(0.6));
			break;
		case 33:
			send(new Throttle(0.7));
			break;
		case 34:
			if(turboAvailable) send(new Turbo());
			else send(new Throttle(1.0));
			break;
		case 35:
			send(new Throttle(1.0));
			break;
		case 36:
			send(new Throttle(1.0));
			break;
		case 37:
			send(new Throttle(1.0));
			break;
		case 38:
			send(new Throttle(1.0));
			break;
		case 39:
			send(new Throttle(1.0));
			break;
		default:
			break;
		}
	}

	private void decideNextAction(MyCar mycar, double thlottle,
			boolean turboAvailable) {
		if (turboAvailable && mycar.turbo) {
			send(new Turbo());
			mycar.turbo = false;
		} else if (mycar.st == SwitchTarget.RIGHT) {
			send(new SwitchLane("Right"));
		} else if (mycar.st == SwitchTarget.LEFT) {
			send(new SwitchLane("Left"));
		} else {
			send(new Throttle(thlottle));
		}
	}

	/**
	 * Main class for participate in another tracks.
	 * 
	 * @param reader
	 * @param writer
	 * @param join
	 * @throws IOException
	 */
	public Main(final BufferedReader reader, final PrintWriter writer,
			final JoinRace join) throws IOException {
		this.writer = writer;
		String line = null;

		send(join);

		// generate mycar
		MyCar mycar = new MyCar();
		LogData logData = new LogData();
		String myCarName;
		boolean turboAvailable = false;
		double thlottle = 0.7;
		mycar.setSlottle(thlottle);
		// Setting for logging.
		System.setOut(new PrintStream(new FileOutputStream(COURSE_NAME + "_"
				+ ".json")));

		while ((line = reader.readLine()) != null) {
			final MsgWrapper msgFromServer = gson.fromJson(line,
					MsgWrapper.class);

			if (msgFromServer.msgType.equals("carPositions")) {
				// set present position in my car
				myCarName = mycar.id.name;
				CarPositions carPositions = gson.fromJson(line,
						CarPositions.class);
				Data carPositionsData = carPositions
						.getSpecifiedDataForCarNamed(myCarName);
				logData.appendData(carPositionsData);

				/* Get variables. */
				double angle = carPositionsData.getAngle();
				PiecePosition currentPiecePosition = carPositionsData
						.getPiecePosition();
				decideAction(currentPiecePosition, turboAvailable);

				// /* Set variables. */
//				 mycar.setAngle(angle);
//				 mycar.setPiecePosition(currentPiecePosition);
//				 mycar.decideTurbo();
//				 mycar.decideSwitchTarget();
//				
//				 thlottle = mycar.decideSpeed();
//				 mycar.setSlottle(thlottle);
//				
//				 decideNextAction(mycar, thlottle, turboAvailable);
			} else if (msgFromServer.msgType.equals("turboAvailable")) {
				// System.out.println("Turbo available!");
				turboAvailable = true;
			} else if (msgFromServer.msgType.equals("join")) {
				// System.out.println("Joined");
			} else if (msgFromServer.msgType.equals("gameInit")) {
				// System.out.println("Race init");
				// set track info in my car
				Course course = gson.fromJson(gson.toJson(msgFromServer.data),
						Course.class);
				mycar.setRaceInfo(course.race.track);
				logData.setCourseInfo(course);
			} else if (msgFromServer.msgType.equals("gameEnd")) {
				// System.out.println("Race end");
				String jsonForLogging = gson.toJson(logData);
				System.out.println(jsonForLogging);
			} else if (msgFromServer.msgType.equals("gameStart")) {
				send(new Throttle(1.0));
				// System.out.println("Race start");
			} else if (msgFromServer.msgType.equals("yourCar")) {
				// set own car info in my car
				YourCar yourcar = gson.fromJson(
						gson.toJson(msgFromServer.data), YourCar.class);
				System.out.println(yourcar);
				mycar.setMyCarInfo(yourcar);
			} else if (msgFromServer.msgType.equals("crash")) {
				// System.out.println("Crash!");
			} else if (msgFromServer.msgType.equals("spawn")) {
				// System.out.println("Spawn!");
			} else if (msgFromServer.msgType.equals("lapFinished")) {
				// System.out.println("Lap finished");
			} else {
				send(new Ping());
			}
		}
	}

	private void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
	}
}

abstract class SendMsg {
	public String toJson() {
		return new Gson().toJson(new MsgWrapper(this));
	}

	protected Object msgData() {
		return this;
	}

	protected abstract String msgType();
}

class MsgWrapper {
	public final String msgType;
	public final Object data;

	MsgWrapper(final String msgType, final Object data) {
		this.msgType = msgType;
		this.data = data;
	}

	public MsgWrapper(final SendMsg sendMsg) {
		this(sendMsg.msgType(), sendMsg.msgData());
	}
}

class Join extends SendMsg {
	public final String name;
	public final String key;

	Join(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	protected String msgType() {
		return "join";
	}
}

class JoinRace extends SendMsg {
	public final BotId botId;
	public final String trackName;
	public final String password;
	public final int carCount;

	JoinRace(final String name, final String key, String trackName, int carCount) {
		this.botId = new BotId(name, key);
		this.trackName = trackName;
		this.password = null;
		this.carCount = carCount;
	}

	class BotId {
		public final String name;
		public final String key;

		public BotId(final String name, final String key) {
			this.name = name;
			this.key = key;
		}
	}

	@Override
	protected String msgType() {
		// TODO Auto-generated method stub
		return "joinRace";
	}
}

class Ping extends SendMsg {
	@Override
	protected String msgType() {
		return "ping";
	}
}

class Throttle extends SendMsg {
	private double value;

	public Throttle(double value) {
		this.value = value;
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "throttle";
	}
}

class SwitchLane extends SendMsg {
	private String lane;

	public SwitchLane(String lane) {
		this.lane = lane;
	}

	@Override
	protected Object msgData() {
		return lane;
	}

	@Override
	protected String msgType() {
		return "switchLane";
	}
}

class Turbo extends SendMsg {
	private String msg;

	public Turbo() {
		this.msg = "Fireeeeeeeeeeeeeeeeee!";
	}

	@Override
	protected Object msgData() {
		return msg;
	}

	@Override
	protected String msgType() {
		return "turbo";
	}
}
