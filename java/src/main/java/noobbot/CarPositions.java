package noobbot;

import java.util.Iterator;
import java.util.List;

/**
 * CarPositions class: This class has current race situations.
 * 
 * @author Taketo Sasaki
 *
 */
public class CarPositions {
	private String msgType;
	private List<Data> data;
	
	public CarPositions() {
		
	}

	
	public String getMsgType() {
		return msgType;
	}



	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}


	public List<Data> getData() {
		return data;
	}

	public void setData(List<Data> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "CarPositions [data=" + data + "]";
	}
	
	public PiecePosition getCurrentPiecePositionOfCarNamed(String carName) {
		Data myCarData = getSpecifiedDataForCarNamed(carName);
		return myCarData.getPiecePosition();
	}
	
	public Data getSpecifiedDataForCarNamed(String carName) {
		Iterator<Data> itr = data.iterator();
		while(itr.hasNext()) {
			Data curData = itr.next();
			if (curData.getId().name.equals(carName)) {
				return curData;
			}
		}
		// Error case.
		return null;
	}
}

class Id {
	private String name;
	private String color;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	@Override
	public String toString() {
		return "Id [name=" + name + ", color=" + color + "]";
	}
	
	
}


class PiecePosition {
	private int pieceIndex;
	private double inPieceDistance;
	private Lane lane;
	public int getPieceIndex() {
		return pieceIndex;
	}
	public void setPieceIndex(int pieceIndex) {
		this.pieceIndex = pieceIndex;
	}
	public double getInPieceDistance() {
		return inPieceDistance;
	}
	public void setInPieceDistance(double inPieceDistance) {
		this.inPieceDistance = inPieceDistance;
	}
	public Lane getLane() {
		return lane;
	}
	public void setLane(Lane lane) {
		this.lane = lane;
	}
	@Override
	public String toString() {
		return "PiecePosition [pieceIndex=" + pieceIndex + ", inPieceDistance="
				+ inPieceDistance + ", lane=" + lane + "]";
	}
	
	
}

class Lane {
	private int startLaneIndex;
	private int endLaneIndex;
	public int getStartLaneIndex() {
		return startLaneIndex;
	}
	public void setStartLaneIndex(int startLaneIndex) {
		this.startLaneIndex = startLaneIndex;
	}
	public int getEndLaneIndex() {
		return endLaneIndex;
	}
	public void setEndLaneIndex(int endLaneIndex) {
		this.endLaneIndex = endLaneIndex;
	}
	@Override
	public String toString() {
		return "Lane [startLaneIndex=" + startLaneIndex + ", endLaneIndex="
				+ endLaneIndex + "]";
	}
	
	
}

class Data {
	private IdRace id;
	private double angle;
	private PiecePosition piecePosition;
	private int lap;
	public IdRace getId() {
		return id;
	}
	public void setId(IdRace id) {
		this.id = id;
	}
	public double getAngle() {
		return angle;
	}
	public void setAngle(double angle) {
		this.angle = angle;
	}
	public PiecePosition getPiecePosition() {
		return piecePosition;
	}
	public void setPiecePosition(PiecePosition piecePosition) {
		this.piecePosition = piecePosition;
	}
	public int getLap() {
		return lap;
	}
	public void setLap(int lap) {
		this.lap = lap;
	}
	@Override
	public String toString() {
		return "Data [id=" + id + ", angle=" + angle + ", piecePosition="
				+ piecePosition + ", lap=" + lap + "]";
	}
}