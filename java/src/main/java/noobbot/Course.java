package noobbot;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class Course {
	public Race race;
}

class Race {
	public Track track;
	public ArrayList<Car> cars;
	public RaceSession raceSession;
	public StartingPoint startingPoint;
}

class Track {
	public String id;
	public String name;
	public ArrayList<Piece> pieces;
	public ArrayList<LaneRace> lanes;
	public StartingPoint startingPoint;
}

class Piece {
	public double length = 0.0;
	@SerializedName("switch")
	public boolean switchFlag = false;
	public double radius = 0.0;
	public double angle = 0.0;
}

class LaneRace {
	public int distanceFromCenter;
	public int index;
}

class StartingPoint {
	public Position position;
	public double angle;
}

class Position {
	public double x;
	public double y;
}

class Car {
	public IdRace id;
	public Dimensions dimension;
}

class IdRace {
	public String name;
	public String color;

	public IdRace(String name, String color) {
		this.name = name;
		this.color = color;
	}
}

class Dimensions {
	public double length;
	public double width;
	public double guideFlagPosition;
}

class RaceSession {
	public int durationMs;
}
