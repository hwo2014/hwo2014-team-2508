package noobbot;

import java.util.ArrayList;
import java.util.List;

public class LogData {
	public MyCarData data;

	public LogData() {
		this.data = new MyCarData();
	}

	public void setCourseInfo(Course course) {
		this.data.course = course;
	}

	public void appendData(Data data) {
		this.data.appendData(data);
	}
}

class MyCarData {
	public Course course;
	public List<Data> carPosition;

	public MyCarData() {
		carPosition = new ArrayList<Data>();
	}

	public void appendData(Data data) {
		this.carPosition.add(data);
	}
}
